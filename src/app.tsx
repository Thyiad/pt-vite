import React, { FC } from 'react';
import { render, hydrate } from 'react-dom';
import { createRoot, hydrateRoot } from 'react-dom/client';
import { AppBrowser } from '@/components/AppContainer';

// render(<AppBrowser />, document.getElementById('root'));
const container = document.getElementById('root');
const root = createRoot(container);
root.render(<AppBrowser />);
