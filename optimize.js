const chalk = require('chalk');
const execSync = require('child_process').execSync;

const getSimpleTime = (span) => {
    const oneSecond = 1000;
    const oneMinu = 1000 * 60;

    if (span < oneSecond) {
        return `${span}ms`;
    }

    if (span < oneMinu) {
        return `${span / oneSecond}s`;
    }

    return `${span / oneMinu}分钟`;
};

console.log(chalk.blue('vite 正在 optimize...'));
const start = Date.now();

execSync('vite optimize', { cwd: __dirname, stdio: 'inherit' });
const end = Date.now();
const span = end - start;

const simpleTime = getSimpleTime(span);
console.log(chalk.blue(`vite optimize 结束，耗时 ${simpleTime}`));
