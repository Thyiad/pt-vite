import path from 'path';
import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
// import react from '@vitejs/plugin-react-swc';
// import legacy from '@vitejs/plugin-legacy';
// import eslintPlugin from '@rollup/plugin-eslint';
// import styleImport from 'vite-plugin-style-import';

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        // { ...eslintPlugin({ include: 'src/**/*.+(js|jsx|ts|tsx)' }), enforce: 'pre' },
        react(),
        // styleImport({
        //     libs: [
        //         {
        //             libraryName: 'antd',
        //             esModule: true,
        //             resolveStyle: (name) => {
        //                 return `antd/es/${name}/style/index`;
        //             },
        //         },
        //     ],
        // }),
        // legacy({
        //     targets: ['defaults', 'not IE 11'],
        // }),
    ],
    css: {
        preprocessorOptions: {
            less: {
                javascriptEnabled: true,
            },
        },
    },
    build: {
        manifest: true,
    },
    server: {
        host: '0.0.0.0',
        port: 5173,
        open: false,
    },
    resolve: {
        alias: {
            '@': path.join(__dirname, './src'),
            // '~antd': path.join(__dirname, './node_modules/antd'),
        },
    },
    optimizeDeps: {
        // entries: 'src/app.tsx',
        // include: [
        //     '@thyiad/antd-ui',
        //     '@thyiad/util',
        //     '@loadable/component',
        //     'js-cookie',
        //     'lrz',
        //     'react',
        //     'react-dom',
        //     'react-router-dom',
        //     'react-is',
        //     'antd > dayjs',
        //     '@ant-design/pro-components > classnames',
        // ],
        // exclude: ['antd', '@ant-design/charts', '@ant-design/icons', '@ant-design/pro-components'],
    },
});
